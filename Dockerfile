FROM tensorflow/tensorflow:1.15.2-py3

ARG DEEPSPEECH_CONTAINER_DIR=/opt/deepspeech
ARG DEEPSPEECH_VERSION=0.9.3

# Install OS dependencies
RUN apt-get update && \
    apt-get install --no-install-recommends -y wget ffmpeg && \
    apt-get clean

# Create app directory
RUN mkdir  /app


# Install Python dependencies
RUN pip3 install --upgrade pip



COPY requirements.txt /tmp
RUN pip3 install -r /tmp/requirements.txt
RUN pip3 install flask


RUN wget https://github.com/common-voice/commonvoice-fr/releases/download/fr-v0.6/model_tensorflow_fr.tar.xz
RUN tar -Jxvf  model_tensorflow_fr.tar.xz

RUN pip3 install requests
RUN pwd
RUN ls


# Copy code and configs
COPY . /srv/app
# RUN mkdir engine
WORKDIR /srv/app/engine
RUN mv /kenlm.scorer /srv/app/engine/
RUN mv /output_graph.pbmm /srv/app/engine/
RUN mv /alphabet.txt /srv/app/engine/
RUN ls /srv/app/engine

WORKDIR /srv/app

ENTRYPOINT python3  deepspeech_server/app.py
