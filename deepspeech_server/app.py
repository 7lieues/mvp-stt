import json, threading, requests, logging, os, io
from base64 import b64decode
from pathlib import Path
from pyhocon import ConfigFactory
from flask import Flask, jsonify, request, json, make_response
from engine import SpeechToTextEngine


logger = logging.getLogger(__name__)
logging.basicConfig(level=10) #logging level in an enviroment variable   #int(os.environ['LOG_LEVEL'])

UMBRELLA_SERVICE = os.environ.get('UMBRELLA_SERVICE')
UMBRELLA_PORT = str(os.environ.get('UMBRELLA_PORT'))
UMBRELLA_ENDPOINT = os.environ.get('UMBRELLA_STT_ENDPOINT')
RETOUR_MULTIPLE = os.environ.get('RETOUR_MULTIPLE')

# Load app configs and initialize DeepSpeech model
conf = ConfigFactory.parse_file("/srv/app/application.conf")

#############   initilise SpeechToTextEngine class with STT model    ##############
engine = SpeechToTextEngine(                            
    model_path=Path(conf["deepspeech.model"]).absolute().as_posix(),
    scorer_path=Path(conf["deepspeech.scorer"]).absolute().as_posix(),
)
app = Flask(__name__)


def convert(audio, sender_id="stt_bot@7lieues.io::0::0::speech_to_text"):

    logger.info("sender ID seen : " + sender_id)

    list_conf,list_text = engine.run(audio)         #use rum methode of the initilised objet for the conversion

    if ( RETOUR_MULTIPLE == "ON") :
        transcript_json  =   {"messages": list_text,"conf"  :  list_conf , "sender_id": sender_id}     # prepare output json
    else :
        transcript_json  =   {"message": list_text[0],"conf"  :  list_conf[0] , "sender_id": sender_id}     # prepare output json

    logger.debug("\n\n\n")

    logger.debug("##########################################  ")
    logger.debug(   " RETOUR_MULTIPLE :"   )
    logger.debug(   RETOUR_MULTIPLE   )
    logger.debug(   "Json to send back to umbrella :"   )
    logger.debug(   transcript_json   )
    logger.debug(" ########################################## ")
    
    url_postback = "http://" + UMBRELLA_SERVICE + ":" + UMBRELLA_PORT + UMBRELLA_ENDPOINT
    logger.debug(" URL UMBRELLA :  " + url_postback)
    ## POSTING BACK TO UMBRELLA ##
    requests.post(url = url_postback, json =    transcript_json      )


    ############################         umbrella postback      ################################################
    # # sending post request and saving response as response object
    # r = requests.post(url = "http://" + UMBRELLA_SERVICE + ":4000" + UMBRELLA_ENDPOINT, json = json_body_test)
    # # extracting response text
    # pastebin_url = r.text
    # print("The pastebin URL is:%s"%pastebin_url)
    #############################################################################################################

    return (transcript_json)



@app.route("/ping")
def pong():
    return "pong"


@app.route( conf['server.stt_endpoint'] , methods=['POST'] )
def stt():
    # On récupère la clé supplémentaire dans la requête :
    user_tracker = request.form["sender_id"]
    uploaded_file = request.files['voice_file']
    if uploaded_file:
        # Le champ filename peut être trouvé en complément sous: uploaded_file.filename
        # Lance STT en background thread (sur la fonction convert()) pour pouvoir rendre la main à la requête réseau et ne pas faire attendre Umbrella
        t1 = threading.Thread(target=convert,args=(uploaded_file.read(), user_tracker))
        t1.start()
    else:
        logger.error("#### Fichier envoyé par Umbrella VIDE !? ####")
    logger.debug("***** running STT inference in background *****")
    return "ok"


if __name__ == "__main__":
    app.run(
        host=conf["server.http.host"],
        port=conf["server.http.port"],
        debug=True,
    )
