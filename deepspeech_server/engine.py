"""Module containing speech-to-text transcription functionality"""
import wave
from io import BytesIO

import ffmpeg
import numpy as np
from deepspeech import Model

import logging
logger = logging.getLogger(__name__)

def normalize_audio(audio):        # convert audio format to .wav with sample_rate= 16000 which is the requires format of mozilla engine
    """Normalize the audio into the format required by DeepSpeech"""

    try:
        out, err = (
        ffmpeg.input("pipe:0")
        .output(
            "pipe:1",
            f="WAV",
            acodec="pcm_s16le",
            ac=1,
            ar="16k",
            loglevel="error",
            hide_banner=None,
        )
        .run(input=audio, capture_stdout=True, capture_stderr=True)
        )
        return ("ok", out)
    except ffmpeg.Error as e:
        logger.debug('stdout:', e.stdout.decode('utf8'))
        logger.debug('stderr:', e.stderr.decode('utf8'))
        # raise e
        return ("error", e.stderr.decode('utf8'))


class SpeechToTextEngine:        # deepspeech framework use the given models to  perform speech-to-text transcription 
    """Class to perform speech-to-text transcription and related functionality"""
    def __init__(self, model_path, scorer_path):
        self.model = Model(model_path=model_path)
        self.model.enableExternalScorer(scorer_path=scorer_path)
        self.model.setScorerAlphaBeta(0.5919543900530122 , 1.6082513974258137 )
        ############### rasa achat ######################

        self.model.addHotWord("fournisseurs",5.3)
        self.model.addHotWord("lister",5.3)
        self.model.addHotWord("Liste",5.3)
        self.model.addHotWord("commandes",5.3)
        self.model.addHotWord("factures",5.3)
        self.model.addHotWord("contacts",5.3)
        self.model.addHotWord("réceptions",5.3)
        self.model.addHotWord("Affiche",5.3)
        self.model.addHotWord("financières",5.3)
        self.model.addHotWord("mail",5.3)
        self.model.addHotWord("téléphone",5.3)
        self.model.addHotWord("numero",5.3)
        self.model.addHotWord("premier",5.3)
        self.model.addHotWord("deuxieme",5.3)
        self.model.addHotWord("dernière",5.3)
        self.model.addHotWord("commercial",5.3)
        self.model.addHotWord("connaître",5.3)
        self.model.addHotWord("reste",5.3)
        self.model.addHotWord("combien",5.3)
        self.model.addHotWord("Zoom",5.3)
        self.model.addHotWord("description",5.3)
        self.model.addHotWord("paiement",5.3)
        self.model.addHotWord("création",5.3)
        self.model.addHotWord("adresse",5.3)
        self.model.addHotWord("nom",5.3)
        self.model.addHotWord("statut",5.3)
        self.model.addHotWord("poste",5.3)
        self.model.addHotWord("livraison",5.3)
        self.model.addHotWord("fermeture",5.3)
        self.model.addHotWord("bénéficiaire",5.3)
        self.model.addHotWord("dépense",5.3)
        self.model.addHotWord("moins",5.3)
        self.model.addHotWord("plus",5.3)
        self.model.addHotWord("gros",5.3)
        self.model.addHotWord("grand",5.3)
        self.model.addHotWord("chiffre",5.3)
        self.model.addHotWord("affaire",5.3)
        self.model.addHotWord("montant",5.3)

        self.model.addHotWord("bonjour",5.3)
        self.model.addHotWord("salut",5.3)
        self.model.addHotWord("achat",5.3)
        self.model.addHotWord("acheter",5.3)
        self.model.addHotWord("souhaite",5.3)
        self.model.addHotWord("effectuer",5.3)
        self.model.addHotWord("annuler",5.3)
        self.model.addHotWord("demande",5.3)
        self.model.addHotWord("arreter",5.3)
        self.model.addHotWord("démarches",5.3)
        self.model.addHotWord("Oui",5.3)
        self.model.addHotWord("Non",5.3)

        ############### rasa achat ######################
        self.model.addHotWord("congé",5.3)
        self.model.addHotWord("demander",5.3)
        self.model.addHotWord("poser",5.3)
        self.model.addHotWord("prendre",5.3)
        self.model.addHotWord("infos",5.3)
        self.model.addHotWord("détails",5.3)
        self.model.addHotWord("solde",5.3)
        self.model.addHotWord("nombre",5.3)
        self.model.addHotWord("vacances",5.3)
        self.model.addHotWord("équipe",5.3)
        self.model.addHotWord("équipes",5.3)
        self.model.addHotWord("validés",5.3)
        self.model.addHotWord("débutés",5.3)
        self.model.addHotWord("initialisé",5.3)
        self.model.addHotWord("refusé",5.3)
        self.model.addHotWord("cours",5.3)
        self.model.addHotWord("attente",5.3)
        self.model.addHotWord("Logistique",5.3)
        self.model.addHotWord("abandonné",5.3)
        self.model.addHotWord("abandonnées",5.3)
        self.model.addHotWord("catalogue",5.3)
        self.model.addHotWord("abscence",5.3)
        self.model.addHotWord("imprévu",5.3)
        self.model.addHotWord("déclarer",5.3)
        self.model.addHotWord("frais",5.3)
        self.model.addHotWord("formation",5.3)
        self.model.addHotWord("Montre",5.3)
        self.model.addHotWord("Travel",5.3)


        ############### rasa TDS ######################

        self.model.addHotWord("Inscription",5.3)
        self.model.addHotWord("inscrire",5.3)
        self.model.addHotWord("voudrais",5.3)
        self.model.addHotWord("possible",5.3)
        self.model.addHotWord("procéder",5.3)
        self.model.addHotWord("souhaiterais",5.3)
        self.model.addHotWord("moderne",5.3)
        self.model.addHotWord("Lead",5.3)
        self.model.addHotWord("Cavalier",5.3)
        self.model.addHotWord("Leader",5.3)
        self.model.addHotWord("Follow",5.3)
        self.model.addHotWord("Cavalière",5.3)
        self.model.addHotWord("Intermédiaire",5.3)
        self.model.addHotWord("Confirmé",5.3)
        self.model.addHotWord("Avancé",5.3)
        self.model.addHotWord("Club",5.3)
        self.model.addHotWord("Nation",5.3)
        self.model.addHotWord("Paris",5.3)
        self.model.addHotWord("Danse",5.3)
        self.model.addHotWord("Studio",5.3)
        self.model.addHotWord("Huchette",5.3)
        self.model.addHotWord("brésilien",5.3)
        self.model.addHotWord("Conflans",5.3)
        self.model.addHotWord("Lundi",5.3)
        self.model.addHotWord("Mardi",5.3)
        self.model.addHotWord("Mercredi",5.3)
        self.model.addHotWord("Jeudi",5.3)
        self.model.addHotWord("continue",5.3)
        self.model.addHotWord("Site",5.3)
        self.model.addHotWord("oublié",5.3)
        self.model.addHotWord("Propriétaire",5.3)
        self.model.addHotWord("Tierce",5.3)
        self.model.addHotWord("personne",5.3)
        self.model.addHotWord("especes",5.3)
        self.model.addHotWord("Chèque",5.3)
        self.model.addHotWord("Virement",5.3)
        self.model.addHotWord("bancaire",5.3)
        self.model.addHotWord("carte",5.3)
        self.model.addHotWord("question",5.3)
        self.model.addHotWord("information",5.3)
        self.model.addHotWord("fonctionnement",5.3)
        self.model.addHotWord("divers",5.3)
        self.model.addHotWord("essais",5.3)
        self.model.addHotWord("complet",5.3)
        self.model.addHotWord("plein",5.3)
        self.model.addHotWord("Récupération",5.3)
        self.model.addHotWord("demarrage",5.3)
        self.model.addHotWord("Méthodes",5.3)
        self.model.addHotWord("Facturation",5.3)
        self.model.addHotWord("Modalité",5.3)
        self.model.addHotWord("Réduction",5.3)
        self.model.addHotWord("Remboursement",5.3)
        self.model.addHotWord("Rembourser",5.3)
        self.model.addHotWord("Absence",5.3)
        self.model.addHotWord("rock",5.3)
        self.model.addHotWord("expérimenté",5.3)
        self.model.addHotWord("merde",5.3)
        self.model.addHotWord("naze",5.3)
        self.model.addHotWord("pourri",5.3)
        self.model.addHotWord("craint",5.3)
        self.model.addHotWord("comprends",5.3)
        self.model.addHotWord("danseur",5.3)
        self.model.addHotWord("gueule",5.3)
        self.model.addHotWord("saoule",5.3)
        self.model.addHotWord("gave",5.3)
        self.model.addHotWord("stop",5.3)
        self.model.addHotWord("plante",5.3)
        self.model.addHotWord("chier",5.3)
        self.model.addHotWord("mauvais",5.3)
        self.model.addHotWord("bugué",5.3)
        self.model.addHotWord("soucis",5.3)
        self.model.addHotWord("déconne",5.3)
        self.model.addHotWord("perdu",5.3)
        self.model.addHotWord("besoin",5.3)
        self.model.addHotWord("putain",5.3)
        self.model.addHotWord("aide",5.3)
        self.model.addHotWord("vraie",5.3)
        self.model.addHotWord("produit",5.3)
        self.model.addHotWord("ça",5.3)

        ############### rasa TDS ######################
        self.model.addHotWord("prénom",5.3)
        self.model.addHotWord("appelle",5.3)
        self.model.addHotWord("comment",5.3)
        self.model.addHotWord("jeune",5.3)
        self.model.addHotWord("age",5.3)
        self.model.addHotWord("Origines",5.3)
        self.model.addHotWord("blague",5.3)
        self.model.addHotWord("connais",5.3)
        self.model.addHotWord("situation",5.3)
        self.model.addHotWord("langue",5.3)
        self.model.addHotWord("parle",5.3)
        self.model.addHotWord("date",5.3)
        self.model.addHotWord("construit",5.3)
        self.model.addHotWord("createur",5.3)
        self.model.addHotWord("programmé",5.3)
        self.model.addHotWord("Programmeur",5.3)
        self.model.addHotWord("inefficace",5.3)
        self.model.addHotWord("inutile",5.3)
        self.model.addHotWord("incapable",5.3)
        self.model.addHotWord("merci",5.3)
        self.model.addHotWord("sport",5.3)
        self.model.addHotWord("humain",5.3)
        self.model.addHotWord("intelligence",5.3)
        self.model.addHotWord("artificielle",5.3)
        self.model.addHotWord("famille",5.3)
        self.model.addHotWord("habites",5.3)
        self.model.addHotWord("émotions",5.3)
        self.model.addHotWord("sentiments",5.3)
        self.model.addHotWord("faire",5.3)
        self.model.addHotWord("actualités",5.3)
        self.model.addHotWord("récemment",5.3)
        self.model.addHotWord("articles",5.3)
        self.model.addHotWord("dernières",5.3)
     
  
    def run(self, audio):
        """Perform speech-to-text transcription"""
        (status, audio) = normalize_audio(audio)
        if status == "error":
            return audio # we should come back to Umbrella with some meaning (bad audio quality, else)
        audio = BytesIO(audio)
        with wave.Wave_read(audio) as wav:
            audio = np.frombuffer(wav.readframes(wav.getnframes()), np.int16)

        # result = self.model.stt(audio_buffer=audio)

        import time
        begin = time.time()

        # result = self.model.stt(audio_buffer=audio)
        result = self.model.sttWithMetadata(audio_buffer=audio,num_results=10)
    
        end = time.time()
        logger.debug(end - begin)

        logger.debug("################################################################################")
        # logger.debug(result.transcripts[1].tokens[0])
        # logger.debug(result.transcripts[1].tokens[0].text)
        list_conf =[ transcript.confidence for transcript in result.transcripts  ]
        list_text=[ "".join([token.text for token in transcript.tokens] ) for transcript in result.transcripts  ]

        for i in range (len (list_conf) ) :
            logger.debug(list_conf[i])
            logger.debug(list_text[i])

        logger.debug("################################################################################")


        return (list_conf,list_text ) # we should come back to Umbrella with some sentence (could we have confidence to be stored ?)
